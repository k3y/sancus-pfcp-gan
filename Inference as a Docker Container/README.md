# Running the attack

To create a Docker container from the Docker image, you can use the docker run command. The general syntax of the command is as follows:

Assuming you want to run the Session Deletion or Modification scripts:

`docker run --network=host -it <image_name> <script_name> <IP_src> <IP_dst> <Interface> <No_Samples>`

e.g.,:

`docker run --network=host -it registry.gitlab.com/k3y/sancus-pfcp-gan Generate_synthetic_data-INT.py 3.45.20.103 3.45.20.102 wlp2s0 100`
