# Introduction
This repository focuses on a smart fuzzer that leverages Generative Adversarial Networks (GANs) to generate malicious PFCP (Packet Forwarding Control Protocol) packets. It specifically targets the N4 interface between the User Plane Function (UPF) and the Session Management Function (SMF) in a 5G core environment. The pre-trained GANs were trained with the attacks of the 5GC PFCP Intrusion Detection Dataset which was produced in the context of the SANCUS project. This Dataset is available in Zenodo: https://zenodo.org/record/7888347#.ZFpKutJBxhE. In particular, three GANs were trained with respect to the PFCP Session Modification DoS Attack, PFCP Session Establishment DoS Attack and PFCP Session Deletion DoS Attack.

# Description of the SANCUS GAN-based Attack Generator

![inference_desc.png](./images/inference_desc.png)


# Requirements

The following python modules are required for running this script:
1. sdv
`pip3 install sdv`

2. pandas
`pip3 install pandas`

3. scapy
`pip3 install scapy`

4. tcpreplay
`apt-get install tcpreplay`

# Execution
The script can be simply executed using a set of input parameters:

1. Source IP address: the ip address from which the synthetic PFCP packets are to be sent (presumably the SMF).

2. Destination IP address: the ip address to which the synthetic PFCP packets are to be sent (presumably the UPF).

3. Interface: the interface where the synthetic PFCP packets will be sent (presumably eth0, in a containerized testbed)

For example:

`python3 reconstruct.py 172.21.0.107 172.21.0.110 eth0`

